let API_PREFIX_URL = "http://localhost:5005/api/v1";

if (process.env.NODE_ENV === 'production') {
  API_PREFIX_URL = 'https://levart.herokuapp.com/api/v1';
}

export const postTravelApi = `${API_PREFIX_URL}/travel/`;