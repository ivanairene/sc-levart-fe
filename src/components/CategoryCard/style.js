import styled from 'styled-components';

export const CategoryCardContainer = styled.div`
  width: 8rem;
  height: 9rem;
  display: flex;
  flex-direction: column;
  border: 1px solid ${props => props.theme.colors.grey};
  border-radius: 4px;
  box-shadow: rgba(0, 0, 0, 0.06) 0px 2px 4px 0px;
  ${props => props.selected && `
    border: 2px solid ${props.theme.colors.darkGrey};
  `}

  &:hover {
    opacity: 0.8;
  }


  .image {
    width: 100%;
    height: 6rem;
    border-radius: 4px 4px 0 0;
    object-fit: cover;
    border-bottom: 1px solid ${props => props.theme.colors.grey};
  }

  .title {
    margin: 0.4rem;
    font-size: 0.7rem;
    font-weight: 600;
    color: ${props => props.theme.colors.black};
  }

  @media screen and (max-width: 768px) {
    width: 6rem;
    height: 8rem;
  }
`