import React from 'react';
import { NavigationContainer } from './style';
import Logo from 'assets/logo.png';

class Navigation extends React.Component {
  render() {
    return (
      <NavigationContainer>
        <img className="logo" src={Logo} />
        <span className="title">Levart</span>
      </NavigationContainer>
    );
  }
}

export default Navigation;