import styled from 'styled-components';

export const NavigationContainer = styled.div`
  width: 100%;
  padding: 1rem 1.5rem;
  display: flex;
  align-items: center;
  border-bottom: 1px solid ${props => props.theme.colors.grey};

  .logo {
    width: 3rem;
    height: 3rem;
    margin: 0 0.5rem 0 0;
    object-fit: contain;
  }

  .title {
    margin: 0;
    font-size: 2rem;
    font-weight: 700;
    color: ${props => props.theme.colors.black};
  }
`