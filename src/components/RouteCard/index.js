import React from 'react';
import PropTypes from 'prop-types';
import { RouteCardContainer } from './style';
import FlightIcon from 'assets/flight.png';
import TrainIcon from 'assets/train.png';

class CategoryCard extends React.Component {

  formatPrice(price) {
    return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
  }

  render() {
  const { origin, destination, method, ticketPrice } = this.props;

    return (
      <RouteCardContainer>
        <div className="originDestinationContainer">
          {
            method !== "total" &&
              <img className="method" src={method === 'pesawat' ? FlightIcon : TrainIcon}/>
          }
          <span className="originDestination">{origin} {method !== 'total' ? "-" : null} {destination}</span>
        </div>
        <span className="originDestination">IDR {this.formatPrice(ticketPrice)}</span>
      </RouteCardContainer>
    );
  }
}

CategoryCard.propTypes = {
  destination: PropTypes.string,
  method: PropTypes.string,
  origin: PropTypes.string,
  ticketPageUrl: PropTypes.string,
  ticketPrice: PropTypes.string
};

export default CategoryCard;