import styled from 'styled-components';

export const RouteCardContainer = styled.div`
  width: 100%;
  padding: 1rem;
  display: flex;
  justify-content: space-between;
  align-items: center;
  border: 1px solid ${props => props.theme.colors.grey};
  border-radius: 4px;
  box-shadow: rgba(0, 0, 0, 0.06) 0px 2px 4px 0px;
  margin: 0 0 1rem;

  &:hover {
    opacity: 0.8;
  }

  .originDestinationContainer {
    display: flex;
    flex-direction: row;
    align-items: center;
  }

  .originDestination {
    font-size: 1rem;
    font-weight: 700;
    color: ${props => props.theme.colors.black};
  }

  .method {
    width: 1.5rem;
    height: 1.5rem;
    object-fit: contain;
    margin: 0 1rem 0 0;
  }
`