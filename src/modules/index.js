/* eslint-disable */
import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import homePageReducer from 'containers/HomePage/reducer';

export default combineReducers({
  'routing': routerReducer,
  'homePageReducer': homePageReducer
});
