import { fromJS } from 'immutable';

import {
  POST_TRAVEL_INFO,
  POST_TRAVEL_INFO_SUCCESS,
  POST_TRAVEL_INFO_FAILED,
} from './constants.js';

const initialState = fromJS({
  error: null,
  isLoaded: false,
  isLoading: false,
  travelRoutes: []
});

function homePageReducer(state = initialState, action) {
  switch (action.type) {
    case POST_TRAVEL_INFO:
      return state.set('isLoading', true)
    case POST_TRAVEL_INFO_SUCCESS:
      return state.set('travelRoutes', action.payload).set('isLoading', false)
    case POST_TRAVEL_INFO_FAILED:
      return state.set('error', action.payload).set('isLoading', false)
    default:
      return state;
  }
}

export default homePageReducer;