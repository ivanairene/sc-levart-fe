import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Select from 'react-select';
import { HomePageContainer } from './style';
import Navigation from 'components/Navigation';
import CategoryCard from 'components/CategoryCard';
import RouteCard from 'components/RouteCard';

import BeachView from 'assets/beachview.jpeg';
import MountainView from 'assets/mountainview.jpeg';
import CityView from 'assets/city.jpeg';
import Culinary from 'assets/culinary.jpeg';
import Loading from 'assets/loading.gif';
import Empty from 'assets/empty.gif';

import { postTravel } from './actions';

import { ORIGIN_CITIES } from './constants';

class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 0,
      travelInfo: {
        budget: '',
        category: '',
        origin: '',
      }
    }
  }

  onChangeInfo = (field, value) => {
    const currentState = this.state.travelInfo;
    currentState[field] = value;
    this.setState({ travelInfo: currentState });
  }

  onClickNext = () => {
    const { currentStep } = this.state;
    if (currentStep === 2) {
      this.props.postTravel(this.state.travelInfo)
    }
    this.setState({ currentStep: currentStep + 1 });
  }

  onClickBack = () => {
    const { currentStep } = this.state;
    this.setState({ currentStep: currentStep - 1 });
  }

  showNextButton = () => {
    const { currentStep, travelInfo } = this.state;

    return (currentStep === 0 && travelInfo.category) ||
      (currentStep === 1 && travelInfo.budget) ||
      (currentStep === 2 && travelInfo.origin);
  }

  showBackButton = () => {
    const { currentStep } = this.state;

    return currentStep > 0;
  }

  getCategoryCards = () => {
    const currentCategory = this.state.travelInfo.category;
    const categories = [
      {
        category: 'beach',
        image: BeachView,
        selected: currentCategory === 'beach',
        title: 'Beach view',
      },
      {
        category: 'mountain',
        image: MountainView,
        selected: currentCategory === 'mountain',
        title: 'Mountain view',
      },
      {
        category: 'city',
        image: CityView,
        selected: currentCategory === 'city',
        title: 'City View',
      },
      {
        category: 'culinary',
        image: Culinary,
        selected: currentCategory === 'culinary',
        title: 'Culinary',
      }
    ]

    return (
      categories.map((item, index) => (
        <button key={index} onClick={() => this.onChangeInfo("category", item.category)}>
          <CategoryCard image={item.image} title={item.title} selected={item.selected} />
        </button>
      ))
    );
  }

  getResults = () => {
    const routes = this.props.travelRoutes;
    let total = 0;

    return (
      <React.Fragment>
        {
          this.props.isLoading
          ? <div className="loadingContainer"><img src={Loading} className="loading" /></div>
          : this.props.travelRoutes.length === 0 ? <div className="loadingContainer"><img src={Empty} className="loading" /></div>
          : <React.Fragment>
              {
                routes.map((item, index) => {
                    total += item.ticketPrice;

                    return (
                      <a href={item.ticketPageUrl} key={index} target="_blank" rel="noopener noreferrer">
                        <RouteCard
                          origin={item.origin}
                          destination={item.destination}
                          method={item.method}
                          ticketPrice={item.ticketPrice}
                        />
                      </a>
                    )
                })
              }
              <RouteCard
                origin="Total"
                method="total"
                ticketPrice={total}
              />
            </React.Fragment>
        }
      </React.Fragment>
    );
  }

  renderFirstStep = () => (
    <div className="innerContentContainer">
      <p className="subtitle">
        Hi there, what kind of travel are you looking for?
      </p>
      <div className="categoryContainer">
        {this.getCategoryCards()}
      </div>
    </div>
  )

  renderSecondStep = () => (
    <div className="innerContentContainer">
      <p className="subtitle">
        Looks great! How much money are you willing to spend?
      </p>
      <input
        type="number"
        className="priceInputField"
        placeholder = "Enter money in IDR"
        onChange={(evt) => this.onChangeInfo("budget", evt.target.value)}
        value={this.state.travelInfo.budget}
      />
    </div>
  )

  renderThirdStep = () => (
    <div className="innerContentContainer">
      <p className="subtitle">
        Awesome! Which city are you travelling from?
      </p>
      <Select
        className="citySelect"
        value={this.state.travelInfo.origin}
        onChange={(option) => this.onChangeInfo("origin", option)}
        options={ORIGIN_CITIES}
      />
    </div>
  )

  renderResults = () => (
    <div className="innerContentContainer">
      <p className="subtitle">
        {
          this.props.isLoading ? "Please wait... we are planning an awesome trip for you..."
          : this.props.travelRoutes.length === 0 ? "Oh noes, We can't find anything! Maybe be more lenient to your tight budget?"
          : "We looked for a while and found that this trip will fit you!"
        }
      </p>
      <div className="resultsContainer">
        {this.getResults()}
      </div>
    </div>
  )

  renderContent = () => {
    const { currentStep } = this.state;
    if (currentStep === 0) {
      return this.renderFirstStep();
    } else if (currentStep === 1) {
      return this.renderSecondStep();
    } else if (currentStep === 2) {
      return this.renderThirdStep();
    } else if (currentStep === 3) {
      return this.renderResults()
    }

    return null;
  }

  render() {
    return (
      <React.Fragment>
        <Navigation />
        <HomePageContainer>
          <div className="content">
            <div className="backButtonContainer">
              {
                this.showBackButton() &&
                  <button onClick={this.onClickBack} className="backButton">Back</button>
              }
            </div>
            {this.renderContent()}
            <div className="nextButtonContainer">
              {
                this.showNextButton() &&
                  <button onClick={this.onClickNext} className="nextButton">Next</button>
              }
            </div>
          </div>
        </HomePageContainer>
      </React.Fragment>
    );
  }
}

HomePage.propTypes = {
  isLoading: PropTypes.bool,
  postTravel: PropTypes.func,
  travelRoutes: PropTypes.shape()
}

function mapStateToProps(state) {
  return {
    isLoaded: state.homePageReducer.get("isLoaded"),
    isLoading: state.homePageReducer.get("isLoading"),
    travelRoutes: state.homePageReducer.get("travelRoutes")
  }
}

function mapDispatchToProps(dispatch) {
  return {
    postTravel: (data) => dispatch(postTravel(data))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);