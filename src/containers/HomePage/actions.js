import axios from 'axios';
import { postTravelApi } from 'api';

import {
  POST_TRAVEL_INFO,
  POST_TRAVEL_INFO_SUCCESS,
  POST_TRAVEL_INFO_FAILED,
} from './constants';

export function postTravel(data) {
  return (dispatch) => {
    dispatch({ type: POST_TRAVEL_INFO });

    return setTimeout(() => {
      axios.post(postTravelApi, data)
      .then((response) => {
        dispatch({
          payload: response.data,
          type: POST_TRAVEL_INFO_SUCCESS,
        })
      })
      .catch((error) => {
        dispatch({
          payload: error,
          type: POST_TRAVEL_INFO_FAILED,
        })
      })
    }, 2000);
  }
}

