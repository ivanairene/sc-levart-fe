export const DEFAULT_ACTION = 'src/HomePage/DEFAULT_ACTION';
export const POST_TRAVEL_INFO = 'src/HomePage/POST_TRAVEL_INFO';
export const POST_TRAVEL_INFO_SUCCESS = 'src/HomePage/POST_TRAVEL_INFO_SUCCESS';
export const POST_TRAVEL_INFO_FAILED = 'src/HomePage/POST_TRAVEL_INFO_FAILED';

export const ORIGIN_CITIES = [
  {
    label: 'Banda Aceh',
    value: 'Banda Aceh'
  },
  {
    label: 'Medan',
    value: 'Medan'
  },
  {
    label: 'Padang',
    value: 'Padang',
  },
  {
    label: 'Pekanbaru',
    value: 'Pekanbaru',
  },
  {
    label: 'Jambi',
    value: 'Jambi',
  },
  {
    label: 'Palembang',
    value: 'Palembang',
  },
  {
    label: 'Bengkulu',
    value: 'Bengkulu',
  },
  {
    label: 'Bandar Lampung',
    value: 'Bandar Lampung',
  },
  {
    label: 'Pangkalpinang',
    value: 'Pangkalpinang',
  },
  {
    label: 'Tanjungpinang',
    value: 'Tanjungpinang',
  },
  {
    label: 'Jakarta',
    value: 'Jakarta',
  },
  {
    label: 'Bandung',
    value: 'Bandung',
  },
  {
    label: 'Semarang',
    value: 'Semarang',
  },
  {
    label: 'Yogyakarta',
    value: 'Yogyakarta',
  },
  {
    label: 'Surabaya',
    value: 'Surabaya',
  },
  {
    label: 'Serang',
    value: 'Serang',
  },
  {
    label: 'Denpasar',
    value: 'Denpasar',
  },
  {
    label: 'Mataram',
    value: 'Mataram',
  },
  {
    label: 'Kupang',
    value: 'Kupang',
  },
  {
    label: 'Pontianak',
    value: 'Pontianak',
  },
  {
    label: 'Palangkaraya',
    value: 'Palangkaraya',
  },
  {
    label: 'Banjarmasin',
    value: 'Banjarmasin',
  },
  {
    label: 'Samarinda',
    value: 'Samarinda',
  },
  {
    label: 'Tanjung Selor',
    value: 'Tanjung Selor',
  },
  {
    label: 'Manado',
    value: 'Manado',
  },
  {
    label: 'Palu',
    value: 'Palu',
  },
  {
    label: 'Makassar',
    value: 'Makassar',
  },
  {
    label: 'Kendari',
    value: 'Kendari',
  },
  {
    label: 'Gorontalo',
    value: 'Gorontalo',
  },
  {
    label: 'Mamuju',
    value: 'Mamuju',
  },
  {
    label: 'Ambon',
    value: 'Ambon',
  },
  {
    label: 'Sofifi',
    value: 'Sofifi',
  },
  {
    label: 'Jayapura',
    value: 'Jayapura',
  },
  {
    label: 'Manokwari',
    value: 'Manokwari',
  },
];