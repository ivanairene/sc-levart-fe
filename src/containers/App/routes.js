import NotFoundPage from 'containers/NotFoundPage';
import HomePage from 'containers/HomePage';

export const routes = [
  {
    'component': HomePage,
    'exact': true,
    'path': '/'
  },
  { component: NotFoundPage }
];
